//
//  RootViewController.m
//  UseExistViewCreateController
//
//  Created by mfw on 14-7-29.
//  Copyright (c) 2014年 MFW. All rights reserved.
//

#import "RootViewController.h"

@interface RootViewController ()<MyViewDataSource,MyViewDelegate>

//@property

@end

@implementation RootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    MyView *view = [[MyView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 54, 320, 54)];
    view.delegate = self;
    view.dataSouces = self;
    view.backgroundColor = [UIColor grayColor];
    [view initWithTypeLbl:@"需支付：" price:@"100" buttonTitle:@"button"];
    [self.view addSubview:view];
    
} 

- (UIButton *)buttonTitle{
    NSLog(@"%s",__func__);
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"马上付款" forState:UIControlStateNormal];
    btn.backgroundColor = [UIColor redColor];
    return btn;
}

- (void)sendMessage
{
    NSLog(@"马上付款");
}

@end
