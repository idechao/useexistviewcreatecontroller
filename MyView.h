//
//  MyView.h
//  UseExistViewCreateController
//
//  Created by mfw on 14-7-29.
//  Copyright (c) 2014年 MFW. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MyViewDelegate <NSObject>

- (void)sendMessage;

@end

@protocol MyViewDataSource <NSObject>

- (UIButton *)buttonTitle;

@end

@interface MyView : UIView

@property (nonatomic, assign) id<MyViewDelegate> delegate;
@property (nonatomic, assign) id<MyViewDataSource> dataSouces;



- (void)initWithTypeLbl:(NSString *)type price:(NSString *)price buttonTitle:(NSString *)title;

@end
