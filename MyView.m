//
//  MyView.m
//  UseExistViewCreateController
//
//  Created by mfw on 14-7-29.
//  Copyright (c) 2014年 MFW. All rights reserved.
//

#import "MyView.h"

//规定view大小:320*54

@interface MyView ()

@property (nonatomic, strong) UIButton *btn;

@end

@implementation MyView{
//    UIButton *_btn;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)initWithTypeLbl:(NSString *)type price:(NSString *)price buttonTitle:(NSString *)title
{
    UILabel *typeLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, (54-24)/2, 64, 24)];
    typeLbl.text = type;
    typeLbl.font = [UIFont fontWithName:@"Helvetica" size:16];
//    typeLbl.textAlignment = NSTextAlignmentJustified;
    typeLbl.font = [UIFont boldSystemFontOfSize:16];
    
    [self addSubview:typeLbl];
    
    UILabel *priceLbl = [[UILabel alloc] initWithFrame:CGRectMake(74, 11, 160-74, 30)];
    priceLbl.text = price;
    priceLbl.backgroundColor = [UIColor orangeColor];
    priceLbl.font = [UIFont fontWithName:@"Helvetica" size:25];

    [self addSubview:priceLbl];
    
    self.btn = [self.dataSouces buttonTitle];
    NSLog(@"%@",self.btn.titleLabel.text);
    self.btn.frame = CGRectMake(160, 10, 150, 35);
    [self.btn addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.btn];
    
}

- (void)buttonClick:(UIButton *)button
{
    [self.delegate sendMessage];
}

@end
