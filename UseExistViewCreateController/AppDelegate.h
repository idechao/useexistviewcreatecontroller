//
//  AppDelegate.h
//  UseExistViewCreateController
//
//  Created by mfw on 14-7-29.
//  Copyright (c) 2014年 MFW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
